(ns biz.calendar
  (:import [java.time DayOfWeek LocalDate Month MonthDay]
           [java.time.temporal ChronoField TemporalAdjusters]))

;; This library only supports years from 2000 to 2099. This allows us
;; to extend 2 digit years to 4 without ambiguity.
(defn year-supported? [y] (< 2000 y 2099))

(defn century-extend-two-digit-year [y]
  (if (<= 0 y 99)
    (+ 2000 y)
    y))

;; A few utilities for working with functions
(defn flip [f] (fn [a b] (f b a)))
(defn fexception [f v] (fn [& args] (try (apply f args) (catch Exception e v))))

;; We lightly use the java.time library, but mostly without support
;; for time zones or hour/minute/second values.
;;
;; These functions let us stick with Clojure-looking values in most
;; places.

(def month
  {:january   Month/JANUARY
   :february  Month/FEBRUARY
   :march     Month/MARCH
   :april     Month/APRIL
   :may       Month/MAY
   :june      Month/JUNE
   :july      Month/JULY
   :august    Month/AUGUST
   :september Month/SEPTEMBER
   :october   Month/OCTOBER
   :november  Month/NOVEMBER
   :december  Month/DECEMBER})

;; A month/day is something like a birthday. "Feb 14" is the same
;; regardless of year. When we stick to "quarter" and "half" then the
;; business calendar mostly acts like month/day.

(defn month-day [m d]
  (MonthDay/of (month m) d))

(defn year-month-day
  ([y md]
   (year-month-day y (.getMonth md) (.getDayOfMonth md)))
  ([y m d]
   (LocalDate/of y m d)))

(defn epoch-day
  [^LocalDate d]
  (.getLong d ChronoField/EPOCH_DAY))

(defn local-date [^long eday] (LocalDate/ofEpochDay eday))

;; These functions let us extract parts of the date represented by a
;; long epoch-day
(defn year-of
  "Return the integer year of the epoch-day eday"
  [eday]
  (.getYear (local-date eday)))

(defn month-of
  "Return the integer month of the epoch-day eday, as 1 - 12"
  [eday]
  (.getMonthValue (local-date eday)))

(defn half-of
  "Return 1 of the epoch-day eday is in Jan 1 - Jun 30, 2 otherwise"
  [eday]
  (if (<= 1 (month-of eday) 6) 1 2))

(defn quarter-of
  "Return 1 - 4 for the quarter that contains the epoch-day eday"
  [eday]
  (let [m (month-of eday)]
    (cond
      (<= 1 m 3)   1
      (<= 4 m 6)   2
      (<= 7 m 9)   3
      (<= 10 m 12) 4)))

;; Business calendar "times" are each ranges of start to end.
;;
;; E.g., a "day" is a range over a 24 hour period. A quarter is a
;; range from the first second of the first day to the last second of
;; the last day. This is called an "interval"
;;
;; We represent intervals as vectors of 2 elements, the start and end
;;
;; Intervals are always inclusive

(defn point-inside?
  "Test if a point is enclosed within the _inclusive_ boundaries"
  [p [lb ub]]
  (<= lb p ub))

(defn quantize-with
  [xrange xlookup x]
  {:pre [(point-inside? x xrange)]}
  (xlookup x))

(def start-of first)
(def end-of   second)

;; There's a lot of string parsing, since values like "3Q19" don't fit
;; into databases well.

(def safe-int (fexception #(Integer/parseInt %) nil))

(def q-pat #"^([1-4])Q([0-9]{4}|[0-9]{2})")
(def h-pat #"^([1-2])H([0-9]{4}|[0-9]{2})")
(def y-pat #"^([0-9]{4}|[0-9]{2})")
(def futures-pat #"^([FGHJKMNQUVXZ])([0-9]{2})")

;; We need a parser to make intervals from strings.
(defmulti interval
  (fn [x]
    (cond
      (nil? x)                                  :interval
      (and (string? x) (re-find q-pat x))       :quarter
      (and (string? x) (re-find h-pat x))       :half
      (and (string? x) (re-find y-pat x))       :year
      (and (string? x) (re-find futures-pat x)) :futures
      (and (sequential? x) (= 2 (count x)))     :interval)))

;; This library doesn't support arbitrary accounting calendars, where
;; one company's "1Q21" might actually run from 2020-November-1
;; through 2021-January-31. (Looking at you retailers!) Or worse, a
;; fiscal quarter that ends on "the last day of February"!

(def q-start
  {1 (month-day :january 1)
   2 (month-day :april   1)
   3 (month-day :july    1)
   4 (month-day :october 1)})

(def q-end
  {1 (month-day :march     31)
   2 (month-day :june      30)
   3 (month-day :september 30)
   4 (month-day :december  31)})

(def h-start
  {1 (month-day :january 1)
   2 (month-day :july    1)})

(def h-end
  {1 (month-day :june      30)
   2 (month-day :december  31)})

(def quarter-start (partial quantize-with [1 4] q-start))
(def quarter-end   (partial quantize-with [1 4] q-end))
(def half-start    (partial quantize-with [1 2] h-start))
(def half-end      (partial quantize-with [1 2] h-end))

(defn q-or-h->interval [pat s startf endf]
  (let [[_ p y] (re-find pat s)
        p (safe-int p)
        y (century-extend-two-digit-year (safe-int y))]
    [(epoch-day (year-month-day y (startf p)))
     (epoch-day (year-month-day y (endf   p)))]))

(defmethod interval :quarter [s]  (q-or-h->interval q-pat s quarter-start quarter-end))
(defmethod interval :half    [s]  (q-or-h->interval h-pat s half-start    half-end))

(defmethod interval :year [s]
  (let [y (century-extend-two-digit-year (safe-int s))]
    [(epoch-day (year-month-day y (month-day :january  1)))
     (epoch-day (year-month-day y (month-day :december 31)))]))

(def ^:private futures-month
  {"F" :january
   "G" :february
   "H" :march
   "J" :april
   "K" :may
   "M" :june
   "N" :july
   "Q" :august
   "U" :september
   "V" :october
   "X" :november
   "Z" :december})

(defn- futures-expiration [m y]
  (.with (year-month-day y m 15)
    (TemporalAdjusters/dayOfWeekInMonth 3 DayOfWeek/FRIDAY)))

(defmethod interval :futures [s]
  (let [[_ m y] (re-find futures-pat s)
        m       (->> m (get futures-month) (get month))
        y   (century-extend-two-digit-year (safe-int y))
        d       (futures-expiration m y)]
    [(epoch-day d) (epoch-day d)]))

;; if the arg is already an interval, pass it through
(defmethod interval :interval [x] x)

(defmethod interval nil
  [x]
  (throw (ex-info (str "Bad date format in '" x "'. Use formats like '1Q20', '2H25', or '2019'.") {:value x})))


;; Intervals form a preorder where a < b means the interval a
;; definitely precedes the interval b. However, `not (a < b)` does
;; NOT imply `b < a`. The intervals may represent overlapping times or
;; one may nest within the interval of the other.

(defn before?
  "Preorder predicate. True if a < b, false otherwise"
  [a b]
  (<= (end-of a) (start-of b)))

(def after? (flip before?))

(defn inside?
  "Nesting predicate. True if a is entirely contained within b.
  Boundaries are inclusive, so an interval is 'inside' itself."
  [a b]
  (and
    (<= (start-of b) (start-of a))
    (<= (end-of a) (end-of b))))

;; Since intervals have a span of time, we can define boolean algebra
;; on those spans.
;;
;; This does introduce the concept of a "null interval" which would be
;; the result of taking the intersection of non-intersecting
;; intervals. We'll use 'nil' to represent that.

;; Clojure's numerics react badly to nil. For our purposes, we can
;; define operations that assign meaning to a nil value.
(defn imin [a b]
  (cond
    (nil? a) b
    (nil? b) a
    :else    (min a b)))

(defn imax [a b]
  (cond
    (nil? a) b
    (nil? b) a
    :else    (max a b)))

(defn intersection
  "Return the interval that is contained in both a and b. May be a null
  interval (nil) if a and b have no intersection."
  [a b]
  (cond
    (nil? a) nil
    (nil? b) nil
    (< (end-of a) (start-of b)) nil
    (< (end-of b) (start-of a)) nil
    :else [(imax (start-of a) (start-of b)) (imin (end-of a) (end-of b))]))

;; Now we need the concept of an irregular interval. That is one which
;; cannot be directly represented as a single string like
;; "1Q20". Think about two quarters that span a half or two halves
;; that span across different years.
;;
;; This will become "interesting" when we try to generate strings to
;; represent intervals.
(defn union
  "Return the interval that contains both a and b. May be irregular."
  [a b]
  [(imin (start-of a) (start-of b)) (imax (end-of a) (end-of b))])


;; We need a way to emit strings that represent an interval. These
;; will not always be precise. That is, it will generally NOT be the
;; case that (= (interval (summarize x)) x)
;;
;; That's because we try to emit strings that are easy for humans to
;; grok. So the string will approximate the interval.

(defn- spans-years?
  [i]
  (not= (year-of (end-of i)) (year-of (start-of i))))

;; This assumes we've already checked for spans-years. So we don't
;; worry about "2H20" versus "2H22"
(defn- spans-halves?
  [i]
  (not= (half-of (end-of i)) (half-of (start-of i))))

;; Likewise here we don't worry about "3Q20" versus "3Q21"
(defn- spans-quarters?
  [i]
  (not= (quarter-of (end-of i)) (quarter-of (start-of i))))

(defn round-to-year    [d] (str (year-of d)))
(defn round-to-half    [d] (str (half-of d) "H" (year-of d)))
(defn round-to-quarter [d] (str (quarter-of d) "Q" (year-of d)))

(defmulti summarize
  (fn [i]
    (cond
      (nil? i)            :nil
      (spans-years? i)    :years
      (spans-halves? i)   :year
      (spans-quarters? i) :half
      :else               :quarter)))

(defmethod summarize :nil     [_] "")
(defmethod summarize :years   [i] (str (year-of (start-of i)) "-" (year-of (end-of i))))
(defmethod summarize :year    [i] (round-to-year (end-of i)))
(defmethod summarize :half    [i] (round-to-half (end-of i)))
(defmethod summarize :quarter [i] (round-to-quarter (end-of i)))

(def ^:private rounders
  {:quarter round-to-quarter
   :half    round-to-half
   :year    round-to-year})

(def ^:private supported-precision? (into #{} (keys rounders)))

(defn detail-at [d prec]
  ((get rounders prec identity) d))

(defn detail
  "Return a string with the endpoints of the interval rounded to the desired precision"
  [i prec]
  {:pre [(supported-precision? prec)]}
  (let [fst (detail-at (start-of i) prec)
        snd (detail-at (end-of i) prec)]
    (if (= fst snd)
      fst
      (str fst "-" snd))))
