(ns biz.calendar-test
  (:require [clojure.test :refer :all]
            [biz.calendar :refer :all]))


(deftest abbreviations
  (testing "two digit years begin from 2000"
    (are [q r] (= (interval q) (interval r))
      "1Q20"   "1Q2020"
      "1Q2020" "1Q20"

      "2H69"   "2H2069"
      "1H99"   "1H2099")))

(deftest interval-is-idempotent
  (are [x] (= (interval x) (interval (interval x)))
    "1Q20"
    "1Q2020"
    "2020"
    nil))


(deftest quarters
  (testing "are a preorder"
    (are [q r] (before? (interval q) (interval r))
      "1Q20" "2Q20"
      "2Q20" "3Q20"
      "3Q20" "4Q20"
      "1Q20" "3Q20"
      "2Q20" "4Q20"

      "4Q20" "1Q21"
      "1Q20" "1Q21")

    (are [q r] (after? (interval r) (interval q))
      "1Q20" "2Q20"
      "2Q20" "3Q20"
      "3Q20" "4Q20"
      "1Q20" "3Q20"
      "2Q20" "4Q20"

      "4Q20" "1Q21"
      "1Q20" "1Q21")))

(deftest nesting
  (testing "quarters"
    (testing "nest inside themselves"
      (are [q] (inside? (interval q) (interval q))
        "1Q20"
        "4Q99"
        "1Q88"))
    (testing "nest inside halves"
      (are [q r] (inside? (interval q) (interval r))
        "1Q20" "1H20"
        "2Q20" "1H20"
        "3Q20" "2H20"
        "4Q20" "2H20"

        "1Q21" "1H21")

      (are [q r] (not (inside? (interval q) (interval r)))
        "1Q20" "2H20"
        "1Q20" "2H99"
        "3Q21" "1H21"))

    (testing "nest inside years"
      (are [q r] (inside? (interval q) (interval r))
        "1Q20" "2020"
        "2Q20" "2020"
        "1Q99" "2099")

      (are [q r] (not (inside? (interval q) (interval r)))
        "1Q20" "2022"
        "2Q20" "2021")))

  (testing "halves nest inside years"
    (are [q r] (inside? (interval q) (interval r))
      "1H20" "2020"
      "2H20" "2020"
      "1H99" "2099")

    (are [q r] (not (inside? (interval q) (interval r)))
      "1H20" "2022"
      "2H20" "2021")

    (are [q] (inside? (interval q) (interval q))
      "2020"
      "2099"
      "88")))

(deftest cross-level-ordering
  (are [q r] (before? (interval q) (interval r))
    "1Q20" "2Q20"
    "1Q20" "2H20"))

(deftest boolean-operations
  (testing "intersection"
    (are [q r s] (= (interval s) (intersection (interval q) (interval r)))
      "3Q20" "3Q20" "3Q20"
      "1H21" "1H21" "1H21"

      "1H20" "1Q20" "1Q20"
      "2020" "2H20" "2H20"

      "1H20" "2H20" nil
      "1Q21" "4Q20" nil

      "1H20" nil    nil
      nil    "2H20" nil))

  (testing "union"
    (are [q r s] (= (interval s) (union (interval q) (interval r)))
      "3Q35" "3Q35" "3Q35"
      "1H20" "2H20" "2020"

      "1Q20" "2Q20" [(start-of (interval "1Q20")) (end-of (interval "2Q20"))]
      "2H20" "1H21" [(start-of (interval "2H20")) (end-of (interval "1H21"))])))

(defn- inclusive [x y]
  (union (interval x) (interval y)))

(deftest summarization
  (testing "outputs a 2 digit year for quarters and halves"
    (are [q r] (= q (summarize (interval r)))
      "1Q2020"   "1Q20"
      "2Q2029"   "2Q29"
      "3Q2000"   "3Q00"
      "4Q2001"   "4Q01"

      "1H2025"   "1H25"
      "2H2025"   "2H25"))

  (testing "outputs a 4 digit year for years"
    (are [q r] (= r (summarize (interval q)))
      "2020" "2020"
      "20"   "2020"
      "2098" "2098"
      "99"   "2099"))

  (testing "uses the smallest string feasible"
    (are [s a b] (= s (summarize (inclusive a b)))
      "1H2020"    "1Q20" "2Q20"
      "2020"      "1Q20" "4Q20"
      "2020-2021" "3Q20" "1Q21")))

(deftest futures-trading-months
  (testing "ordered by symbol"
    (are [q r] (before? (interval q) (interval r))
      "F20" "Q20"
      "Z20" "F21")))
