# biz.calendar

Clojure library to work with "planning dates" in a business
calendar. These are fuzzy dates like "fourth quarter 2020."

## Latest version

No releases yet, feel free to use in `deps.edn` as:

```clojure
{:deps {biz.calendar {:git/url "https://gitlab.com/mtnygard/biz.calendar.git"
                      :git/sha "d914afe149311ab5f6014f977d3aa8bfbe56e16e"}
```

## Usage

Construct an "interval" from as string:

```clojure
(require '[biz.calendar :as c])

(c/interval "1Q2021")
;; => [18628 18717]
```

The interval is represented as a 2-vector of start-date and
end-date. These are "epoch dates", counted as the number of days since
Jan 1, 1970.

You can also use a 2 digit year. In that case, we assume it is in the
2000's.

```clojure
(c/interval "3Q20")
;; => [18444 18535]
```

You can test whether an interval entirely precedes another:

```clojure
(c/before? (c/interval "1Q20") (c/interval "3Q20"))
;; => true

o(c/before? (c/interval "3Q20") (c/interval "1Q20"))
;; => false

(c/before? (c/interval "1Q20") (c/interval "1H20"))
;; => false
```

First quarter of 2020 does _not_ precede the first half of 2020,
because it is part of it.

We support boolean operations on these intervals

```clojure
(c/intersection (c/interval "1H20") (c/interval "1Q20"))
;; => [18262 18352]

(c/intersection (c/interval "1Q21") (c/interval "1Q22"))
;; => nil
```

A nil interval means there is no overlap. Functions on intervals
should all "do something reasonable" when fed a nil. If you ever get
an exception, that's a definite bug. Please report an issue.

Union also works.

```clojure
(c/union (c/interval "1Q20") (c/interval "2Q20"))
;; => [18262 18443]

(c/union (c/interval "1Q20") (c/interval "1Q21"))
;; => [18262 18717]
```

To get a string back from an interval, use `summarize`.

```clojure
(c/summarize (c/interval "1Q21"))
;; => "1Q2021"
```

This is *not* the dual of `interval`. `summarize` can and will return
values that do not parse. That is because `summarize` is meant for
planning purposes. It produces human-consumable representations,
because we don't think in epoch-days and we don't plan our businesses
using them.

In particular, once you start using `union`, you can create an
interval that doesn't neatly fit into something like "3Q20".

```clojure
(c/summarize (c/union (c/interval "1Q21") (c/interval "2Q21")))
;; => "1H2021"

(c/summarize (c/union (c/interval "1Q21") (c/interval "2H21")))
;; => "2021"

(c/summarize (c/union (c/interval "1Q21") (c/interval "1Q22")))
;; => "2021-2022"
```

Obviously we lose some fidelity, especially when plans span multiple
years.

If you want more precision, you can ask for the explicit range, rounded
to whatever precision you like:

```clojure
(c/detail (c/interval "1Q21") :quarter)
;; => "1Q2021"

(c/detail (c/interval "1Q21") :half)
;; => "1H2021"

(c/detail (c/interval "1Q21") :year)
;; => "2021"

(c/detail (c/union (c/interval "3Q21") (c/interval "2Q22")) :quarter)
;; => "3Q2021-2Q2022"

(c/detail (c/union (c/interval "3Q21") (c/interval "2Q22")) :year)
;; => "2021-2022"
```

## Copyright

Copyright (C) 2020 Michael T. Nygard

## License

Distributed under the Eclipse Public License
